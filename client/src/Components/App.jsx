import React from 'react';
import { hot } from 'react-hot-loader';
import { Search } from 'semantic-ui-react';

const App = () => (
  <div>
    <Search />
  </div>
);

export default hot(module)(App);
