import config
import spotipy
import sys
import pprint
from spotipy.oauth2 import SpotifyClientCredentials
client_credentials_manager = SpotifyClientCredentials(client_id=config.client_id, client_secret=config.client_secret)

if len(sys.argv) > 1:
    search_str = sys.argv[1]
else:
    search_str = 'Radiohead'

# sp = spotipy.Spotify()
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
result = sp.search(search_str)
pprint.pprint(result)

# import spotipy
# sp = spotipy.Spotify()
#
# results = sp.search(q='weezer', limit=20)
# for i, t in enumerate(results['tracks']['items']):
#     print ' ', i, t['name']
